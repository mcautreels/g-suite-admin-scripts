var regexStripParenthesis = /\([^\)]*?(?:(?:('|")[^'"]*?\1)[^\)]*?)*\)/g;

function processQueryBasedGroups() {
  console.log('Query Based Groups Script started');
  
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("List of Groups");
  var data = sheet.getDataRange().getValues();
  var groupEntries = [];
  
  for (var i = 1; i < data.length; i++) {
    
    var groupEntry = {
      displayName: data[i][0],
      upn: data[i][1],
      query: data[i][2] 
    }
    
    groupEntries.push(groupEntry);
  }
  
  for(var i = 0; i < groupEntries.length; i++) {
    var groupEntry = groupEntries[i];
    createGroup(groupEntry.upn, groupEntry.displayName);
    addUsersToGroup(groupEntry.upn, groupEntry.query);
  }
  console.log('Query', groupEntry.query);
  console.log('Query Based Groups Script finished');
}

function createGroup(groupUPN, groupDisplayName) {
  var group = {
    email: groupUPN,
    name: groupDisplayName
  }
  try {
    AdminDirectory.Groups.insert(group);
  } catch (error) {
    if(error.message == 'API call to directory.groups.insert failed with error: Entity already exists.') {
      console.log('Group ' + groupUPN + ' already exists.');
    } else {
      throw error;
    }    
  }
}

function getGroupMembers(groupUPN) {
  var membersList = [];
  var pageToken, page;
  
  do {
    page = AdminDirectory.Members.list(groupUPN, {
      pageToken: pageToken
    });
    
    var members = page.members;
    if (members) {
      membersList = membersList.concat(page.members);
    } else {
      console.log('Group ' + groupUPN + ' has no members.');
    }
    pageToken = page.nextPageToken;
  } while (pageToken);
  
  return membersList;
}

function getMembersForQuery(groupQuery) {
  
  var futureGroupMembers = [];
  
  var queryParts = groupQuery.split(' OR ');
  
  for(var queryPartIndex=0;queryPartIndex<queryParts.length;queryPartIndex++) {
    var queryPart = queryParts[queryPartIndex];
    var notquery;
        
    var myStr = 'this,is,a,test';
var newStr = myStr.replace(/,/g, '-');
    
    var hasNOTStatement = queryPart.indexOf(' NOT ') > 0;
    if(hasNOTStatement) {
      var queryWithNOTParts = queryPart.split(' NOT ');
      queryPart = queryWithNOTParts[0];
      var notPart = queryWithNOTParts[1];
      
      notPart = notPart.replace(/[\(\)]*/g, '')  + ' isSuspended=false';
      
      console.log('NOT Query: ' + notPart);
      
      notQuery = queryPart + notPart + ' isSuspended=false';
    }
    
   
    /*var n = queryPart.search(regexStripParenthesis);
    queryPart = queryPart.substring(n + 1, queryPart.length - 1) + ' isSuspended=false';*/
    
    queryPart = queryPart.replace(/[\(\)]*/g, '') + ' isSuspended=false';
    
    console.log('Retrieving users for part ' + (queryPartIndex+1) + ': ' + queryPart);
    var queryResult = processQuery(queryPart);
    
    if(hasNOTStatement) {
      var notResult = processQuery(queryPart + ' ' + notPart);
      
      for(var i in queryResult) {
        var futureMember = queryResult[i];
        var futureMemberFound = false;
        
        for(var j in notResult) {
          var notMember = notResult[j];
          
          if(futureMember.primaryEmail === notMember.primaryEmail) {
            futureMemberFound = true;
            console.log('Found a match: ' + futureMember.primaryEmail);
            break;
          }
        }
        
        if(!futureMemberFound) {
          futureGroupMembers.push(futureMember); 
        }
      }
    } else {
      futureGroupMembers = futureGroupMembers.concat(queryResult);
    }
  }
    
  return futureGroupMembers;
}

function processQuery(queryPart) {
  var queryResult = [];
  
  var pageToken, page, queryResult = [];
  do {
    page = AdminDirectory.Users.list({
      domain: 'persgroep.net',
      orderBy: 'givenName',
      query: queryPart,
      pageToken: pageToken
    });
    var users = page.users;
    if (users) {
      queryResult = queryResult.concat(page.users);
    } else {
      console.log('Query ' + queryPart + ' returned no users.');
    }
    pageToken = page.nextPageToken;
  } while (pageToken);
  
  return queryResult;
}

/*
 * Process one OR part and add the result to the group
 */
function addUsersToGroup(groupUPN, groupQuery) {
  
  var currentGroupMembers = getGroupMembers(groupUPN);
  var futureGroupMembers = getMembersForQuery(groupQuery);
  
  var membersToAdd = [];
  var membersToRemove = [];
  
  //Find new members
  for(var i = 0; i < futureGroupMembers.length; i++) {
    var futureMemberUPN = futureGroupMembers[i].primaryEmail;
    var futureMemberFound = false;
    
    for(var j = 0; j < currentGroupMembers.length; j++) {
      if(futureMemberUPN == currentGroupMembers[j].email) {
        futureMemberFound = true;
        break;
      }
    }
    
    if(!futureMemberFound) {
      membersToAdd.push(futureMemberUPN);
    }
  }
  
  //Find members to remove
  for(var i = 0; i < currentGroupMembers.length; i++) {
    var currentMemberUPN = currentGroupMembers[i].email;
    var currentMemberFound = false;
    
    for(var j = 0; j < futureGroupMembers.length; j++) {
      if(currentMemberUPN == futureGroupMembers[j].primaryEmail) {
        currentMemberFound = true;
        break;
      }
    }
    
    if(!currentMemberFound) {
      membersToRemove.push(currentMemberUPN);
    }
  }
  
  //Remove members
  for(var i = 0; i < membersToRemove.length; i++) {
    var memberUPN = membersToRemove[i];
    console.log('Removing ' + memberUPN + ' from ' + groupUPN); 
    try {
      AdminDirectory.Members.remove(groupUPN, memberUPN);
    } catch (error) {
      console.log('Trying to remove ' + memberUPN + ' from group ' + groupUPN + ' but it failed with message: ' + error.message); 
    }
  }
  
  //Add new members
  for(var i = 0; i < membersToAdd.length; i++) {
    var memberUPN = membersToAdd[i];
    console.log('Adding ' + memberUPN + ' to ' + groupUPN);
    var member = {
      email: memberUPN,
      role: 'MEMBER'
    };
    try {
      member = AdminDirectory.Members.insert(member, groupUPN);
    } catch (error) {
      if(error.message == 'Member already exists.') {
        console.log('Member ' + memberUPN + ' is already part of the group ' + groupUPN); 
      }
    }
  }
}